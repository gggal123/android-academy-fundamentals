import java.time.LocalDateTime
import java.math.BigDecimal
import java.time.DayOfWeek

class OrdersAnalyzer {
    data class Order(val orderId: Int, val creationDate: LocalDateTime, val orderLines: List<OrderLine>)
    data class OrderLine(val productId: Int, val name: String, val quantity: Int, val unitPrice: BigDecimal)

    fun totalDailySales(orders: List<Order>): Map<DayOfWeek, Int> {
        val quantitiesPerDayMap = mutableMapOf<DayOfWeek, Int>()
        for (order in orders) {
            for (orderline in order.orderLines) {
                val currentDayCount = quantitiesPerDayMap[order.creationDate.dayOfWeek] ?: 0
                quantitiesPerDayMap.put(
                    order.creationDate.dayOfWeek,
                    currentDayCount + orderline.quantity
                )
            }
        }
        return quantitiesPerDayMap
    }
}


val order1Lines1 = OrdersAnalyzer.OrderLine(0, "Cup", 1, BigDecimal.TEN)
val order1Lines2 = OrdersAnalyzer.OrderLine(1, "Spoon", 4, BigDecimal.TEN)
val order1Lines3 = OrdersAnalyzer.OrderLine(2, "Fork", 7, BigDecimal.TEN)
val order1 = OrdersAnalyzer.Order(0, LocalDateTime.now(), listOf(order1Lines1, order1Lines2, order1Lines3))

val order2Lines1 = OrdersAnalyzer.OrderLine(0, "Cup", 1, BigDecimal.TEN)
val order2Lines2 = OrdersAnalyzer.OrderLine(1, "Spoon", 2, BigDecimal.TEN)
val order2Lines3 = OrdersAnalyzer.OrderLine(2, "Fork", 7, BigDecimal.TEN)
val order2 =
    OrdersAnalyzer.Order(0, LocalDateTime.of(2019, 11, 12, 8, 0), listOf(order2Lines1, order2Lines2, order2Lines3))

val ordersList = arrayListOf<OrdersAnalyzer.Order>(order1, order2)

println(OrdersAnalyzer().totalDailySales(ordersList))